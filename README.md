# Michaux de rosette

Tentative de création d'un programme d'identification de lettres à partir des signes aléatoire générés en michaux.

#### Méthode

La méthode de gros bourrin utilisé consiste en la comparaison de matrice d'image. On compare les pixels d'une lettre en michaux avec une lettre en helvetica et on voit si ces les mêmes. Plus on laisse tourner l'algorithme longtemps plus il a le temps de tester de signes plus il trouve des lettre proche du modèle en input. 

#### A développer

Il faudrait arriver à optimiser le programme pour qu'il puisse tourner plus vite, ainsi qu'affiner l'algorithme de sélection des signes.
Il faudrait aussi arriver à exporter des fichiers vectoriel, pour l'instant c'est que du bitmap.