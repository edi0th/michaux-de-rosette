import processing.pdf.*;

// Variables juste pour faire un nom unique de fichier de stockage, ainsi que la variable de random
int d = second();
int m = minute();
int y = hour();
float r() {
  return random (50, 200);
}

// Déclaration de variables tampon nécessitant d'être endehors de tout scope :
// - La variable matrice des signes michaux générés
// - La variable image des signes michaux générés
// - La variable de taux de noir des signes michaux générés
int[] matricemichaux=new int[62500];
PImage c;
int noirlettre=0;

// Déclaration des lettres à chercher (il faut un .jpeg correspondant avec le même nom (ex:a.jpg ou R.jpg) et création de l'array vide avec un nb de cases correspondant.
String[] NomsLettres = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
Lettre[] lettres = new Lettre[NomsLettres.length];

// Variable de paramètres modifiable: Plus nbtour est grand plus on a d'angles testés en rotation.
// Si le premier Boléen est true les résultats de comparaison sont pondérés par un ratio de nombre de points noirs (mais c'est pas trop utile en l'état)
// Si le second Boléen est true on test aussi les signes en symétrie (+ rotations de cette symétrie)
int nbtour = 10;
boolean actiPonderation = false;
boolean actiSymetrie = true;

// P'tit setup Oklm
void setup() {
  size(250, 250); 
  smooth();
  strokeWeight(15);
  strokeCap(SQUARE);
  noFill();
  frameRate(500);

  // Création de tous les objets lettre.
  for (int i=0; i < lettres.length; i++) {
    lettres[i] = new Lettre(NomsLettres[i]);
  }
}

void draw() {
  // Vérifie que la symétrie n'a pas encore été effectuée (si true);
  boolean dejaSym=true;
  background(255);
  michaux();

  // Boucle de rotation
  for (int j=0; j < nbtour; j++) {
    // On enregistre l'image du canevas dans c, on la fait tourne, on la ré-imprime sur le canevas et on stock la matrice dans l'array matricemichaux[].
    c = get(0, 0, 250, 250);
    pushMatrix();
    translate(125, 125); 
    rotate(radians(j*(360/nbtour)));
    image(c, -125, -125);
    popMatrix();
    c = get(0, 0, 250, 250);
    binaryconvector(c.pixels, matricemichaux);

    // On parse toutes les lettres de l'array d'objet lettres, et on test si les signes correspondent.
    for (int i=0; i < lettres.length; i++) {
      // On stock dans noirlettre le nombre de pixels noir de l'image de références. Ne sert que si on active actiPonderation.
      noirlettre = lettres[i].noirceur;

      // On calcule le score de similarité, si celui-ci est meilleur que le dernier score, 
      // ou qu'il est égal à 0 (pour le premier tour), on stock l'image en pdf avec le nom de la lettre.
      // On stock aussi parce qu'un jour ca pourrait servir le nombre d'occurence pour une lettre trouvé jusque là.
      float sc = score(lettres[i].matriceLettre);
      if (sc > lettres[i].dernierScore || lettres[i].dernierScore==0) {
        lettres[i].dernierScore = sc;
        //beginRecord(PDF, "alphabet"+d+m+y+"/"+lettres[i].nomLettre+"/"+lettres[i].nomLettre+""+lettres[i].nombreOccurences+"-"+lettres[i].dernierScore+".pdf"); 
        beginRecord(PDF, "alphabet"+d+m+y+"/"+lettres[i].nomLettre+".pdf"); 
        image(c, 0, 0);
        endRecord();
        lettres[i].nombreOccurences++;
      }
    }

    // Si actiSymetrie est true, on bascule le canevas puis on repart dans la boucle de rotation plus haut en passant j à 0;
    if (actiSymetrie) {
      if (dejaSym && j==nbtour-1) {
        println("pouip");
        j=0;
        pushMatrix();
        scale(-1, 1);
        image(c, -c.width, 0);
        dejaSym=false;
      }

      // Quand on a fini tous les tours de rotation de symétrie on popMatrix();
      if (dejaSym==false && j==nbtour-1) {
        popMatrix();
      }
    }
  }
}

// La fonction originale (copyright 2012) de dessin des signes du michaux.
void michaux() {
  for (int i=0; i<3; i++) {
    if (random(2)<1) {
      bezier(r(), r(), r(), r(), r(), r(), r(), r());
    } else {
      line(r(), r(), r(), r());
    }
  }
}

// Fonction qui prend une matrice image pas très propre et la transforme en une suite de 0 et de 1. Pas forcement hyper utile là j'me dis.
void binaryconvector(int[] matriceinput, int[] matriceoutput) {
  for (int i=0; i<matriceinput.length; i++) {
    if (matriceinput[i]==-16777216) {
      matriceoutput[i]=1;
    } else {
      matriceoutput[i]=0;
    }
  }
}

// Algorithme de comparaison. Hyper simple. Si les pixels d'une même position n dans deux matrices différentes sont égaux, alors 
// on augmente le score de 1 (c-a-d, s'ils sont tout les deux blancs, ou tout le deux noirs). S'ils sont différents, on baisse le
// score de 1. On pourrait aussi laisser à 0, mais je pense que -1 est plus efficace.
float score(int[] matriceinput) {
  float score=0;
  for (int i = 0; i < matricemichaux.length; i++) {
    if (matriceinput[i]==matricemichaux[i]) {
      score+=1;
    } else {
      score-=1;
    }
  }

  // Partie de ponderation avec un ratio qui mesure si les deux images ont des quantité similaires de pixels noirs.
  //  Pas efficace en l'état et désactivé par défaut.
  if (actiPonderation) {
    int noirmichaux = noir(matricemichaux);
    float ponderation; 

    if (noirmichaux == noirlettre) {
      ponderation = 1;
    } else {
      ponderation = abs(noirmichaux-noirlettre);
    }
    score=sqrt(score*(1/ponderation));
    println(noirlettre+" | "+noirmichaux+" | "+" | "+ponderation+" | "+score);
  }
  return score;
}

// Mesure le nombre de pixels noirs dans une matrice.
int noir(int[] matriceinput) {
  int noirceur=0;
  for (int i = 0; i < matricemichaux.length; i++) {
    if (matriceinput[i]==1) {
      noirceur+=1;
    }
  }
  return noirceur;
}

// Classe des objets lettres. On a juste besoin du nom de la lettre en input.
class Lettre {  
  String nomLettre;
  PImage imageLettre;
  int[] matriceLettre = new int[62500];
  float dernierScore;
  int nombreOccurences;
  int noirceur;

  Lettre(String tempNomLettre) {  
    nomLettre = tempNomLettre;  
    imageLettre = loadImage(tempNomLettre+".jpg");
    imageLettre.loadPixels();
    binaryconvector(imageLettre.pixels, matriceLettre);
    dernierScore = 0;
    nombreOccurences = 0;
    noirceur = noir(matriceLettre);
  }
}
